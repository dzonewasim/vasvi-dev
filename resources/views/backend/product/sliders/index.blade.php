@extends('backend.layouts.app')
@section('content')
    @can('slider_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-primary" href="{{ route('admin.sliders.create') }}">
                    Add New
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            <a class="btn btn-primary" href="{{ route('admin.sliders.create') }}">
                Add New
            </a>
        </div>

        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Slider">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Id
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                           Image
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($banners as $ban)
                        
                   
<tr>
    <th>{{$ban->id}}</th>
    <td>{{$ban->title}}</td>
    <td>{{$ban->iamge}}</td>
</tr>
@endforeach
                </tbody>
            </table>
        </div>
    </div>



@endsection
@push('scripts')
   
    
@endpush
