
    <div style="padding-top:60px;">
        <div class="container">
           <div class="row">
              <div class="col-lg-12">
                 <div class="home-product best_seller">
                    <div class="owl-carousel owl-theme  home-product-slider" id="bestSeller">
                     
                        @foreach (filter_products(\App\Product::where('published', 1)->where('latest', 1))->limit(20)->get() as $key => $product)
                        
                            @include('frontend.partials.product_box_1',['product' => $product])
                        
                    @endforeach
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>

