{{-- <div class="aiz-card-box border border-light rounded hov-shadow-md mt-1 mb-2 has-transition bg-white">
    <div class="position-relative">
        <a href="{{ route('product', $product->slug) }}" class="d-block">
            <img
                class="img-fit lazyload mx-auto h-140px h-md-210px"
                src="{{ static_asset('assets/img/placeholder.jpg') }}"
                data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                alt="{{  $product->getTranslation('name')  }}"
                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
            >
        </a>
        <div class="absolute-top-right aiz-p-hov-icon">
            <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
                <i class="la la-heart-o"></i>
            </a>
            <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
                <i class="las la-sync"></i>
            </a>
            <a href="javascript:void(0)" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
                <i class="las la-shopping-cart"></i>
            </a>
        </div>
    </div>
    <div class="p-md-3 p-2 text-left">
        <div class="fs-15">
            @if(home_base_price($product) != home_discounted_base_price($product))
                <del class="fw-600 opacity-50 mr-1">{{ home_base_price($product) }}</del>
            @endif
            <span class="fw-700 text-primary">{{ home_discounted_base_price($product) }}</span>
        </div>
        <div class="rating rating-sm mt-1">
            {{ renderStarRating($product->rating) }}
        </div>
        <h3 class="fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
            <a href="{{ route('product', $product->slug) }}" class="d-block text-reset">{{  $product->getTranslation('name')  }}</a>
        </h3>
        @if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated)
            <div class="rounded px-2 mt-2 bg-soft-primary border-soft-primary border">
                {{ translate('Club Point') }}:
                <span class="fw-700 float-right">{{ $product->earn_point }}</span>
            </div>
        @endif
    </div>
</div> --}}

<div class="item">
    <div class="product-card">
       <div>
          <div id="f1_container">
            <a href="{{ route('product', $product->slug) }}">
             <div id="f1_card" class="shadow">
                <div class="front face">
                    <img
                class="img-fit lazyload mx-auto h-140px h-md-210px"
                src="{{ static_asset('assets/img/placeholder.jpg') }}"
                data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                alt="{{  $product->getTranslation('name')  }}"
                onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
            >
                   
                </div>
                <div class="back face center">
                    <img
                    class="img-fit lazyload mx-auto h-140px h-md-210px"
                    src="{{ static_asset('assets/img/placeholder.jpg') }}"
                    data-src="{{ uploaded_asset($product->thumbnail_img) }}"
                    alt="{{  $product->getTranslation('name')  }}"
                    onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
                >
                </div>
             </div>
            </a>
          </div>
       </div>
       <div class="text-caption">
        <a href="{{ route('product', $product->slug) }}" class="d-block text-reset"><p><strong>  {{  $product->getTranslation('name')  }}</strong></p></a>
          <p> <span class="price-txt">{{ home_discounted_base_price($product) }}</span> <span class="price-oveline">@if(home_base_price($product) != home_discounted_base_price($product)) {{ home_base_price($product) }} @endif</span> <span class="discount-text">
            <?php
                
        $discount_applicable = false;
        $discount='';
if ($product->discount_start_date == null) {
    $discount_applicable = true;
} elseif (strtotime(date('d-m-Y H:i:s')) >= $product->discount_start_date &&
    strtotime(date('d-m-Y H:i:s')) <= $product->discount_end_date) {
    $discount_applicable = true;
}

if ($discount_applicable) {
    if ($product->discount_type == 'percent') {
        $discount=$product->discount.'%';
        
    } elseif ($product->discount_type == 'amount') {
        $discount='Rs'.$product->discount;
    }
}
                ?>      
           {{$discount}}
        </span></p>
          <p class="pro-rating"> 
              
            {{ renderStarRating($product->rating) }}
        
          </p>
       </div>
       <div class="caption-hover">
        {{-- <a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
            <i class="la la-heart-o"></i>
        </a>
        <a href="javascript:void(0)" onclick="addToCompare({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
            <i class="las la-sync"></i>
        </a> --}}
        <a href="javascript:void(0)" class="pinkBtn" onclick="showAddToCartModal({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
            <i class="las la-shopping-cart"></i>
        </a>
          
          <div class="btn-section"><a href="javascript:void(0)" onclick="addToWishList({{ $product->id }})" data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
            <i class="la la-heart-o"></i>
        </a></div>
          <div class="text-caption">
             <p><strong>Kurty</strong> Dry Woven Team Training</p>
          </div>
        
       </div>
       <div class="icon-wishlist"><button type="button"  data-toggle="tooltip" data-placement="left" title="Save for Later"><i class="fas fa-heart"></i></button> </div>
    </div>
 </div>