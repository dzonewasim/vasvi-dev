<!DOCTYPE html>
<html lang="en">
   <head>
      <title></title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="description" content="vasvi shop project">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="app-url" content="{{ getBaseURL() }}">
      <meta name="file-base-url" content="{{ getFileBaseURL() }}">
  
      <title>@yield('meta_title', get_setting('website_name').' | '.get_setting('site_motto'))</title>
  
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="robots" content="index, follow">
      <meta name="description" content="@yield('meta_description', get_setting('meta_description') )" />
      <meta name="keywords" content="@yield('meta_keywords', get_setting('meta_keywords') )">
  
      @yield('meta')
  
      @if(!isset($detailedProduct) && !isset($customer_product) && !isset($shop) && !isset($page) && !isset($blog))
      <!-- Schema.org markup for Google+ -->
      <meta itemprop="name" content="{{ get_setting('meta_title') }}">
      <meta itemprop="description" content="{{ get_setting('meta_description') }}">
      <meta itemprop="image" content="{{ uploaded_asset(get_setting('meta_image')) }}">
  
      <!-- Twitter Card data -->
      <meta name="twitter:card" content="product">
      <meta name="twitter:site" content="@publisher_handle">
      <meta name="twitter:title" content="{{ get_setting('meta_title') }}">
      <meta name="twitter:description" content="{{ get_setting('meta_description') }}">
      <meta name="twitter:creator" content="@author_handle">
      <meta name="twitter:image" content="{{ uploaded_asset(get_setting('meta_image')) }}">
  
      <!-- Open Graph data -->
      <meta property="og:title" content="{{ get_setting('meta_title') }}" />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="{{ route('home') }}" />
      <meta property="og:image" content="{{ uploaded_asset(get_setting('meta_image')) }}" />
      <meta property="og:description" content="{{ get_setting('meta_description') }}" />
      <meta property="og:site_name" content="{{ env('APP_NAME') }}" />
      <meta property="fb:app_id" content="{{ env('FACEBOOK_PIXEL_ID') }}">
      @endif
  
      <!-- Favicon -->
      <link rel="icon" href="{{ uploaded_asset(get_setting('site_icon')) }}">
  
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
  
      <!-- CSS Files -->
      <link rel="stylesheet" href="{{ static_asset('assets/css/vendors.css') }}">
      @if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
      <link rel="stylesheet" href="{{ static_asset('assets/css/bootstrap-rtl.min.css') }}">
      @endif
      <link rel="stylesheet" href="{{ static_asset('assets/css/aiz-core.css') }}">
      <link rel="stylesheet" href="{{ static_asset('assets/css/custom-style.css') }}">
  
  
      <script>
          var AIZ = AIZ || {};
          AIZ.local = {
              nothing_selected: '{{ translate('Nothing selected') }}',
              nothing_found: '{{ translate('Nothing found') }}',
              choose_file: '{{ translate('Choose file') }}',
              file_selected: '{{ translate('File selected') }}',
              files_selected: '{{ translate('Files selected') }}',
              add_more_files: '{{ translate('Add more files') }}',
              adding_more_files: '{{ translate('Adding more files') }}',
              drop_files_here_paste_or: '{{ translate('Drop files here, paste or') }}',
              browse: '{{ translate('Browse') }}',
              upload_complete: '{{ translate('Upload complete') }}',
              upload_paused: '{{ translate('Upload paused') }}',
              resume_upload: '{{ translate('Resume upload') }}',
              pause_upload: '{{ translate('Pause upload') }}',
              retry_upload: '{{ translate('Retry upload') }}',
              cancel_upload: '{{ translate('Cancel upload') }}',
              uploading: '{{ translate('Uploading') }}',
              processing: '{{ translate('Processing') }}',
              complete: '{{ translate('Complete') }}',
              file: '{{ translate('File') }}',
              files: '{{ translate('Files') }}',
          }
      </script>
  
      <style>
          body{
              font-family: 'Open Sans', sans-serif;
              font-weight: 400;
          }
          :root{
              --primary: {{ get_setting('base_color', '#e62d04') }};
              --hov-primary: {{ get_setting('base_hov_color', '#c52907') }};
              --soft-primary: {{ hex2rgba(get_setting('base_color','#e62d04'),.15) }};
          }
  
          #map{
              width: 100%;
              height: 250px;
          }
          #edit_map{
              width: 100%;
              height: 250px;
          }
  
          .pac-container { z-index: 100000; }
      </style>
  
  @if (get_setting('google_analytics') == 1)
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('TRACKING_ID') }}"></script>
  
      <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '{{ env('TRACKING_ID') }}');
      </script>
  @endif
  
  @if (get_setting('facebook_pixel') == 1)
      <!-- Facebook Pixel Code -->
      <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '{{ env('FACEBOOK_PIXEL_ID') }}');
          fbq('track', 'PageView');
      </script>
      <noscript>
          <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id={{ env('FACEBOOK_PIXEL_ID') }}&ev=PageView&noscript=1"/>
      </noscript>
      <!-- End Facebook Pixel Code -->
  @endif

  @php
      echo get_setting('header_script');
  @endphp
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/bootstrap.min-3.3.7.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/menu.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/style-new.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/owl.carousel.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/fontawesome-all.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/front-new.css')}}">
      <link rel="stylesheet" type="text/css" href="{{static_asset('frontend/css/front-new2.css')}}">
   </head>
   <body>
    @php
    if(auth()->user() != null) {
        $user_id = Auth::user()->id;
        $cart = \App\Cart::where('user_id', $user_id)->get();
    } else {
        $temp_user_id = Session()->get('temp_user_id');
        if($temp_user_id) {
            $cart = \App\Cart::where('temp_user_id', $temp_user_id)->get();
        }
    }
  @endphp
   @include('frontend.includes.nav')
   @yield('content')
   @include('frontend.includes.footer')
   @include('frontend.partials.modal')

   <div class="modal fade" id="addToCart">
       <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
           <div class="modal-content position-relative">
               <div class="c-preloader text-center p-3">
                   <i class="las la-spinner la-spin la-3x"></i>
               </div>
               <button type="button" class="close absolute-top-right btn-icon close z-1" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true" class="la-2x">&times;</span>
               </button>
               <div id="addToCart-modal-body">

               </div>
           </div>
       </div>
   </div>

   @yield('modal')
      <!-- SCRIPTS -->
      <script src="{{ static_asset('assets/js/vendors.js') }}"></script>
      <script src="{{ static_asset('assets/js/aiz-core.js') }}"></script>  
      <script src="{{static_asset('frontend/js/jquery.min.js')}}"></script> 
      <script src="{{static_asset('frontend/js/bootstrap.min.js')}}"></script>
      <script src="{{static_asset('frontend/js/owl.carousel.min.js')}}"></script>
      <script src="{{static_asset('frontend/js/megamenu.js')}}"></script>
      
      <script>
         $(document).ready(function(){
             $('[data-toggle="tooltip"]').tooltip();
         });
      </script> 
      <script>
         $(document).ready(function() {
           $('.product-slider .owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             autoplay:true,
             responsiveClass: true,
             responsive: {
               0: {
                 items: 1,
                 nav: true
               },
               600: {
                 items: 2,
                 nav: false
               },
               1000: {
                 items: 4,
                 nav: true,
                 loop: false,
                 margin: 20
               }
             }
           })
         })
      </script>
      <script>
         $(document).ready(function() {
           $('#featuredCat .owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             autoplay:true,
             responsiveClass: true,
             responsive: {
               0: {
                 items: 1,
                 nav: true
               },
               600: {
                 items: 2,
                 nav: false
               },
               1000: {
                 items: 4,
                 nav: true,
                 loop: false,
                 margin: 20
               }
             }
           })
         })
      </script>
      <script>
         $(document).ready(function() {
           $('.best_seller .owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             autoplay:true,
             responsiveClass: true,
             responsive: {
               0: {
                 items: 1,
                 nav: true
               },
               600: {
                 items: 2,
                 nav: false
               },
               1000: {
                 items: 4,
                 nav: true,
                 loop: false,
                 margin: 20
               }
             }
           })
         })
      </script>
      <script>
         $(document).ready(function() {
           $('.vasvi_exclusive_slider .owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             autoplay:true,
             responsiveClass: true,
             responsive: {
               0: {
                 items: 1,
                 nav: true
               },
               600: {
                 items: 2,
                 nav: false
               },
               1000: {
                 items: 4,
                 nav: true,
                 loop: false,
                 margin: 20
               }
             }
           })
         })
      </script>
      <script>
         $(document).ready(function() {
           $('.client-review .owl-carousel').owlCarousel({
             loop: true,
             margin: 10,
             autoplay:true,
             responsiveClass: true,
             responsive: {
               0: {
                 items: 1,
                 nav: true
               },
               600: {
                 items: 2,
                 nav: false
               },
               1000: {
                 items: 2,
                 nav: true,
                 loop: false,
                 margin: 20
               }
             }
           })
         })
      </script>
      <script>
         $(window).scroll(function() {
             if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
                 $('#return-to-top').fadeIn(200); // Fade in the arrow
             } else {
                 $('#return-to-top').fadeOut(200); // Else fade out the arrow
             }
         });
         $('#return-to-top').click(function() { // When arrow is clicked
             $('body,html').animate({
                 scrollTop: 0 // Scroll to top of body
             }, 500);
         });
      </script>
      <script>
         window.onscroll = function() {
             myFunction()
         };
         
         var navbar = document.getElementById("navbar");
         var sticky = navbar.offsetTop;
         
         function myFunction() {
             if (window.pageYOffset >= sticky) {
                 navbar.classList.add("sticky")
             } else {
                 navbar.classList.remove("sticky");
             }
         }
      </script>
      <script>
         $(document).ready(function(){
           $(".signin-btn").click(function(){
            $("#registerModal").hide(); 
           
           });
         
           $(".signup-btn").click(function(){
            $("#loginModal").hide(); 
             
           });
         
          
         
         });
           
           
      </script>
      <script type="text/javascript">
         $(function(){
         $('#chatbox').popover({
          
           placement: 'top',
           title: 'Write a Note',
           html:true,
           content:  $('#myForm').html()
         }).on('click', function(){
         // had to put it within the on click action so it grabs the correct info on submit
         $('.btn-submit-review ').click(function(){
          $('#result').after("form submitted by " + $('#email').val())
           $.post('/echo/html/',  {
               email: $('#email').val(),
               name: $('#name').val(),
               phone: $('#phone').val(),
           }, function(r){
             $('#pops').popover('hide')
             $('#result').html('resonse from server could be here' )
           })
         })
         })
         })
         
         
         
         
      </script>
      <script>
            $('#option-choice-form input').on('change', function(){
            getVariantPrice();
        });

                function getVariantPrice(){
            if($('#option-choice-form input[name=quantity]').val() > 0 && checkAddToCartValidity()){
                $.ajax({
                   type:"POST",
                   url: '{{ route('products.variant_price') }}',
                   data: $('#option-choice-form').serializeArray(),
                   success: function(data){

                        $('.product-gallery-thumb .carousel-box').each(function (i) {
                            if($(this).data('variation') && data.variation == $(this).data('variation')){
                              AIZ.plugins.slickCarousel('slickGoTo', i);
                                // $('.product-gallery-thumb').slick('slickGoTo', i);
                            }
                        })

                       $('#option-choice-form #chosen_price_div').removeClass('d-none');
                       $('#option-choice-form #chosen_price_div #chosen_price').html(data.price);
                       $('#available-quantity').html(data.quantity);
                       $('.input-number').prop('max', data.max_limit);
                       if(parseInt(data.in_stock) == 0 && data.digital  == 0){
                           $('.buy-now').addClass('d-none');
                           $('.add-to-cart').addClass('d-none');
                           $('.out-of-stock').removeClass('d-none');
                       }
                       else{
                           $('.buy-now').removeClass('d-none');
                           $('.add-to-cart').removeClass('d-none');
                           $('.out-of-stock').addClass('d-none');
                       }
                   }
               });
            }
        }
        function checkAddToCartValidity(){
            var names = {};
            $('#option-choice-form input:radio').each(function() { // find unique names
                  names[$(this).attr('name')] = true;
            });
            var count = 0;
            $.each(names, function() { // then count them
                  count++;
            });

            if($('#option-choice-form input:radio:checked').length == count){
                return true;
            }

            return false;
        }
        function showAddToCartModal(id){
            if(!$('#modal-size').hasClass('modal-lg')){
                $('#modal-size').addClass('modal-lg');
            }
            $('#addToCart-modal-body').html(null);
            $('#addToCart').modal();
            $('.c-preloader').show();
            $.post('{{ route('cart.showCartModal') }}', {_token: AIZ.data.csrf, id:id}, function(data){
                $('.c-preloader').hide();
                $('#addToCart-modal-body').html(data);
                AIZ.plugins.slickCarousel();
                AIZ.plugins.zoom();
                AIZ.extra.plusMinus();
                getVariantPrice();
            });
        }
        function addToCart(){
            if(checkAddToCartValidity()) {
                $('#addToCart').modal();
                $('.c-preloader').show();
                $.ajax({
                    type:"POST",
                    url: '{{ route('cart.addToCart') }}',
                    data: $('#option-choice-form').serializeArray(),
                    success: function(data){

                       $('#addToCart-modal-body').html(null);
                       $('.c-preloader').hide();
                       $('#modal-size').removeClass('modal-lg');
                       $('#addToCart-modal-body').html(data.modal_view);
                       AIZ.extra.plusMinus();
                       updateNavCart(data.nav_cart_view,data.cart_count);
                    }
                });
            }
            else{
                AIZ.plugins.notify('warning', "{{ translate('Please choose all the options') }}");
            }
        }

        function buyNow(){
            if(checkAddToCartValidity()) {
                $('#addToCart-modal-body').html(null);
                $('#addToCart').modal();
                $('.c-preloader').show();
                $.ajax({
                   type:"POST",
                   url: '{{ route('cart.addToCart') }}',
                   data: $('#option-choice-form').serializeArray(),
                   success: function(data){
                       if(data.status == 1){

                            $('#addToCart-modal-body').html(data.modal_view);
                            updateNavCart(data.nav_cart_view,data.cart_count);

                            window.location.replace("{{ route('cart') }}");
                       }
                       else{
                            $('#addToCart-modal-body').html(null);
                            $('.c-preloader').hide();
                            $('#modal-size').removeClass('modal-lg');
                            $('#addToCart-modal-body').html(data.modal_view);
                       }
                   }
               });
            }
            else{
                AIZ.plugins.notify('warning', "{{ translate('Please choose all the options') }}");
            }
        }
        function updateNavCart(view,count){
            $('.cart-count').html(count);
            $('#cart_items').html(view);
        }
        function removeFromCart(key){
            $.post('{{ route('cart.removeFromCart') }}', {
                _token  : AIZ.data.csrf,
                id      :  key
            }, function(data){
                updateNavCart(data.nav_cart_view,data.cart_count);
                $('#cart-summary').html(data.cart_view);
                AIZ.plugins.notify('success', "{{ translate('Item has been removed from cart') }}");
                $('#cart_items_sidenav').html(parseInt($('#cart_items_sidenav').html())-1);
            });
        }
        function show_purchase_history_details(order_id)
        {
            $('#order-details-modal-body').html(null);

            if(!$('#modal-size').hasClass('modal-lg')){
                $('#modal-size').addClass('modal-lg');
            }

            $.post('{{ route('purchase_history.details') }}', { _token : AIZ.data.csrf, order_id : order_id}, function(data){
                $('#order-details-modal-body').html(data);
                $('#order_details').modal();
                $('.c-preloader').hide();
            });
        }
        function switchfunc(status) {
          $.ajax({
                   type:"POST",
                   url: '{{ route('user.become.switch') }}',
                   data: {status,_token:'{{csrf_token()}}'},
                   success: function(data){

                   window.location.href='{{route('home')}}'
                   }
               });
        }
      </script>

      @stack('scripts')
   </body>
</html>