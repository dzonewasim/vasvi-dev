@extends('frontend.layouts.app')
@section('content')

 <div class="headertopspace"></div>
 <!--Slider Section start here-->
 <section class="top-banner" style="margin-top: 24px">
   @if (get_setting('home_slider_images') != null)
                        <div class="aiz-carousel dots-inside-bottom mobile-img-auto-height" data-arrows="true" data-dots="true" data-autoplay="true">
                            @php $slider_images = json_decode(get_setting('home_slider_images'), true);  @endphp
                            @foreach ($slider_images as $key => $value)
                                <div class="carousel-box">
                                    <a href="{{ json_decode(get_setting('home_slider_links'), true)[$key] }}">
                                        <img
                                            class="d-block mw-100 img-fit rounded shadow-sm overflow-hidden"
                                            src="{{ uploaded_asset($slider_images[$key]) }}"
                                            alt="{{ env('APP_NAME')}} promo"
                                            
                                            height="457"
                                          
                                            onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder-rect.jpg') }}';"
                                        >
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
 </section>

 <!--Slider Section end here-->
 <!-- New Arrivals Section start here -->
 <section class="top-banner">
    <div class="container">
       <div class="row">
          <div class="col-md-12 heading-title text-center">
             <h2 class="title-txt">Trending</h2>
             <img src="{{static_asset('frontend/images/headline.png')}}">
          </div>
          <div class="col-md-6">
             <div class="row">
                <div class="side-img col-md-12">
                   <img height="90%" class="img-fluid" src="{{static_asset('frontend/images/fmix.jpg')}}" alt="women" />
                </div>
                <div class="side-img col-md-12">
                   <img height="90%" class="img-fluid" src="{{static_asset('frontend/images/easy.jpg')}}" alt="women" />
                </div>
             </div>
          </div>
          <div class="col-md-6">
             <img style="height: 504px;" class="img-fluid" src="{{static_asset('frontend/images/fsuit.jpg')}}" alt="women" />
          </div>
       </div>
    </div>
 </section>
 <!-- New Arrivals Section start here -->>
 <!--Proudct item Slider Start here-->
 <section id="section_trending" class="colorfulbg">

</section>
 <!--Proudct item Slider end here-->

 <!--Best Seller Section start here-->
 <section>
    <div class="container">
       <div class="row">
          <div class="col-md-12 text-center heading-title">
             <h2 class="title-txt">Hot Sales</h2>
             <img src="{{static_asset('frontend/images/headline.png')}}">
          </div>
          <div class="col-xs-12">
             <div class="row">
                <div class="col-sm-6 col-xs-12">
                   <div class="catImg">
                      <img src="{{static_asset('frontend/images/bestsle0.jpg')}}" class="img-responsive">
                   </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                   <div class="catImg">
                      <img src="{{static_asset('frontend/images/bestsle1.jpg')}}" class="img-responsive">
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </section>
 <!--Best Seller Section start here-->
 <!--Proudct item Slider Start here-->

  
 
 <section id="section_hot_sales" class="colorfulbg">

</section>
 <!--Proudct item Slider end here-->
 <div class="col-md-12 text-center heading-title">
   <h2 class="title-txt">Latest</h2>
   <img src="{{static_asset('frontend/images/headline.png')}}">
</div>
 <section class="vasvi-offer-banner"><img src="{{static_asset('frontend/images/banner2.jpg')}}" alt="" /> </section>
  <!--Vasvi Exclusive Slider Start here-->
  <section>

   <div class="exclusive-bg"></div>
   <section id="section_latest" class="colorfulbg">

   </section>
</section>
<!--Vasvi Exclusive Slider end here-->
 <section class="who-we-section">
    <div class="container">
       <div class="row">
          <div class="col-md-12 text-center">
             <div class="col-md-12 text-center heading-title">
                <h2 class="title-txt">Who We Are</h2>
                <img src="{{static_asset('frontend/images/headline.png')}}">
             </div>
             <p class="how-we-p text-center">
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book
             </p>
          </div>
       </div>
    </div>
 </section>
 <!-- Customer Reviews Section start -->
 <section class="client-review">
    <div class="container">
       <div class="row">
          <div class="col-md-12 text-center heading-title">
             <h2 class="title-txt">What our customer says</h2>
             <img src="{{static_asset('frontend/images/headline.png')}}">
          </div>
          <div class="owl-carousel owl-theme col-md-12">
             <div class="item">
                <div class="col-lg-3">
                   <img class="imground client-img"  src="{{static_asset('frontend/./images/user-icon1.png')}}" width="60" alt="customer">
                   <h4 class="client-name">Dzone Gupta</h4>
                   <p class="client-location">Jaipur </p>
                </div>
                <div class="col-lg-9">
                   <div class="testimonial-text">
                      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book</p>
                   </div>
                </div>
             </div>
             <div class="item">
                <div class="col-lg-3">
                   <img class="imground client-img"  src="{{static_asset('frontend/./images/user-icon2.png')}}" width="60" alt="customer">
                   <h4 class="client-name">Manish Sharma</h4>
                   <p class="client-location">Jaipur </p>
                </div>
                <div class="col-lg-9">
                   <div class="testimonial-text">
                      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book</p>
                   </div>
                </div>
             </div>
             <div class="item">
                <div class="col-lg-3">
                   <img class="imground client-img"  src="{{static_asset('frontend/./images/user-icon2.png')}}" width="60" alt="customer">
                   <h4 class="client-name">Manish Sharma</h4>
                   <p class="client-location">Jaipur </p>
                </div>
                <div class="col-lg-9">
                   <div class="testimonial-text">
                      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book</p>
                   </div>
                </div>
             </div>
             <div class="item">
                <div class="col-lg-3">
                   <img class="imground client-img"  src="{{static_asset('frontend/./images/user-icon2.png')}}" width="60" alt="customer">
                   <h4 class="client-name">Manish Sharma</h4>
                   <p class="client-location">Jaipur </p>
                </div>
                <div class="col-lg-9">
                   <div class="testimonial-text">
                      <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.printer took a galley of type and scrambled it to make a type specimen book</p>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </section>
 <!-- Customer Reviews Section end -->
 <!-- Blog Section start here -->
 <div class="container">
    <div class="row  ">
       <div class="col-md-12 text-center heading-title">
          <h2 class="title-txt">Latest Blog</h2>
          <img src="{{static_asset('frontend/images/headline.png')}}">
       </div>
    </div>
    <div class="row justify-content-center ">
       <div class="col-lg-3 col-md-3">
          <div class="home-blog d-block">
             <img class="img-fluid" src="{{static_asset('frontend/./images/blog1.jpg')}}" alt="blog" />
             <div class="home-blog-content">
                <div class="home-blog-title"><a href="#">Trends 2018</a></div>
                <div class="home-blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
             </div>
          </div>
       </div>
       <div class="col-lg-3 col-md-3">
          <div class="home-blog d-block">
             <img class="img-fluid" src="{{static_asset('frontend/./images/blog1.jpg')}}" alt="blog" />
             <div class="home-blog-content">
                <div class="home-blog-title"><a href="#">Trends 2018</a></div>
                <div class="home-blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
             </div>
          </div>
       </div>
       <div class="col-lg-3 col-md-3">
          <div class="home-blog d-block">
             <img class="img-fluid" src="{{static_asset('frontend/./images/blog2.jpg')}}" alt="blog" />
             <div class="home-blog-content">
                <div class="home-blog-title"><a href="#">Trends 2018</a></div>
                <div class="home-blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
             </div>
          </div>
       </div>
       <div class="col-lg-3 col-md-3">
          <div class="home-blog d-block">
             <img class="img-fluid" src="{{static_asset('frontend/./images/blog3.jpg')}}" alt="blog" />
             <div class="home-blog-content">
                <div class="home-blog-title"><a href="#">Trends 2018</a></div>
                <div class="home-blog-text">Lorem ipsum dolor sit amet, consectetur adipiscing Donec et.</div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <!-- Blog Section end here -->
 <!-- Newsletter -->
 <div class="newsletter">
    <div class="container">
       <div class="row">
          <div class="col-md-4">
             <div class="newsletter_title text-left">We accept</div>
             <ul class="we-accept-img">
                <li>
                   <a href="#"><img class="img-fluid" src="{{static_asset('frontend/./images/payment.png')}}" alt="payments"></a>
                </li>
             </ul>
          </div>
          <div class=" col-md-4">
             <div class="footer_column">
                <div class="newsletter_title text-left">Download our app</div>
                <ul class="footer_list">
                   <li>
                      <a href="#"><img class="img-fluid" src="{{static_asset('frontend/./images/ios.png')}}" alt="payments"></a>
                   </li>
                   <li>
                      <a href="#"><img class="img-fluid" src="{{static_asset('frontend/./images/android.png')}}" alt="payments"></a>
                   </li>
                </ul>
             </div>
          </div>
          <div class="col-md-4">
             <div class="newsletter_title text-left">Subscribe for Latest Update</div>
             <div class="newsletter_content clearfix">
                <form action="#" class="newsletter_form">
                   <input type="email" class="newsletter_input" required placeholder="Enter your email address">
                   <button class="newsletter_button">Subscribe</button>
                </form>
             </div>
          </div>
       </div>
    </div>
 </div>

 {{-- Best Seller --}}
 @if (get_setting('vendor_system_activation') == 1)
 <div id="section_best_sellers">

 </div>
 @endif
 

@endsection
@push('scripts')
   <script>
        $(document).ready(function(){
         @if (get_setting('vendor_system_activation') == 1)
            $.post('{{ route('home.section.best_sellers') }}', {_token:'{{ csrf_token() }}'}, function(data){
             
                $('#section_best_sellers').html(data);
                AIZ.plugins.slickCarousel();
            });
            @endif
            $.post('{{ route('home.section.hot.sales') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_hot_sales').html(data);
                setTimeout(() => {
                  $('.owl-carousel').owlCarousel({
loop:true,
margin:10,
nav:true,
responsive:{
    0:{
        items:1
    },
    600:{
        items:3
    },
    1000:{
        items:4
    }
}
});
                }, 100);
       
               //  AIZ.plugins.slickCarousel();
            });
            $.post('{{ route('home.section.trending') }}', {_token:'{{ csrf_token() }}'}, function(data){
               
                $('#section_trending').html(data);
                setTimeout(() => {
                  $('.owl-carousel').owlCarousel({
loop:true,
margin:10,
nav:true,
responsive:{
    0:{
        items:1
    },
    600:{
        items:3
    },
    1000:{
        items:4
    }
}
});
                }, 100);
       
               //  AIZ.plugins.slickCarousel();
            });
            $.post('{{ route('home.section.latest') }}', {_token:'{{ csrf_token() }}'}, function(data){
                $('#section_latest').html(data);
                setTimeout(() => {
                  $('.owl-carousel').owlCarousel({
loop:true,
margin:10,
nav:true,
responsive:{
    0:{
        items:1
    },
    600:{
        items:3
    },
    1000:{
        items:4
    }
}
});
                }, 100);
       
               //  AIZ.plugins.slickCarousel();
            });
        });
   </script>
@endpush