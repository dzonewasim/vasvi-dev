   <!-- OTP Modal Star here -->
   <div class="modal fade" id="OTPModal" tabindex="-1" role="dialog" aria-labelledby="OTPModalLabel" aria-hidden="true">
    <div class="modal-dialog opt-dialog" role="document">
       <div class="modal-content">
          <div class="modal-body">
             <button type="button" class="close opt-close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
             <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                   <h4 class="modal-title mb-4 text-center ">Enter OTP</h4>
                   <p style="font-size:13px; color:#333; margin-bottom:0px; line-height:25px; text-align:center;">A Verification code has been send to your mobile 
                   </p>
                   <div class="form-group mobilenumber-input">
                      <input type="text" class="form-control text-center boldtxt otp-number" value="9929294352"  >
                      <button class="edit-icon edit-btn-new text-danger">
                      <i class="fas fa-pencil-alt"></i></button>
                   </div>
                   <div class="form-group mobilenumber-input">
                      <input type="text" class="form-control text-center boldtxt otp-number" placeholder="Enter Verification code (OTP)"   > 
                   </div>
                   <div>
                      <button type="button" class="btn btn-danger btn-block  f_size13" id="submit">VERIFY</button>
                   </div>
                   <a href="#" class="text-danger text-center mt-4" onclick="resend();">Resend?</a> 
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <!-- OTP Modal Star here -->
 <!-- Sign Up  Modal start here -->
 <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
       <div class="modal-content col-md-12 pad-l0 pad-r0">
          <div class="modal-body login_modal register-modal">
             <div class="row">
                <div class="col-md-12">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                   </button>
                   <div class="pad15">
                      <h2 class="modal_title">Create An Account</h2>
             
                     <ul id="myTabs" class="nav nav-pills nav-pills-signup  nav-justified" role="tablist" data-tabs="tabs">
                     <li class="active"><a href="#Customer" data-toggle="tab">Customer Sign-Up</a></li>
                     <li><a href="#Wholesaler" data-toggle="tab">Wholesaler / Retailer Sign-Up</a></li> 
                     </ul>
                    <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="Customer">
                  <div class="formrow martop15 borbotnone">
                     <div class="col-md-12 text-center">
                        <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-social btnfb facebook">
                           <i class="fab fa-facebook-f"></i> <span class="divider"></span> Login with Facebook
                       </a>
                        
                        <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-social btn-gmail google">
                           <i class="fab fa-google"></i> <span class="divider"></span> Login with Google
                       </a>
                     </div>
                  </div>
                  <div class="row">
                    
                        <form class="col-md-12"  method="POST" action="{{ route('register') }}">
                           @csrf
                        <div class="formrow">
                           <div class="col-lg-12 col-md-12 col-sm-12 iconbg namefield">
                              <input type="text" name="name"  placeholder="First & Last Name" class="form-control"> 
                           </div>
                        </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg emailfield">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
             </div>
          </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="Password" required>
             </div>
          </div>
        
          <div class="formrow">
            <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
               <input  id="password-confirm" type="password" class="form-control" type="Password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
               <a href="#" class="iconviewpass"></a>
            </div>
         </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg locfield">
                <select class="form-control">
                   <option value="">City</option>
                   <option>Jaipur</option>
                </select>
             </div>
          </div>
          <div class="row martop10">
             <div class="col-md-12" style="text-align:center;">
               {{-- <button  class="btn  btn-pink otp-modal " type="button" data-toggle="modal" data-target="#OTPModal" style="cursor: pointer;">Get OTP</button> --}}
               <button  class="btn  btn-pink" type="submit" style="cursor: pointer;">Submit</button>
                
            </div>
             <div class="col-md-12 signuplink">Do you have an account? <a href="JavaScript:void(0);" data-toggle="modal" data-target="#loginModal" class="signin-btn">Sign In</a></div>
          </div>
       </form>
    </div> 
</div>
<div role="tabpanel" class="tab-pane fade" id="Wholesaler">

    <div class="row">
       
      <form class="col-md-12"  style="margin-top: 30px"  method="POST" action="{{ route('register') }}">
         @csrf
         <input type="hidden" name="wholesaler" value="1" >
         <div class="formrow">
            <div class="col-lg-12 col-md-12 col-sm-12 iconbg namefield">
               <input type="text" name="name"  placeholder="First & Last Name" class="form-control"> 
            </div>
         </div>
         <div class="formrow">
            <div class="col-lg-12 col-md-12 col-sm-12 iconbg namefield">
               <input type="text" name="company_name"  placeholder="Company Name" class="form-control"> 
            </div>
         </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg namefield">
                <input type="text" name="contact_person" placeholder="Contact Person (Optional)" class="form-control"> 
             </div>
          </div>
          <div class="formrow">
            <div class="col-lg-12 col-md-12 col-sm-12 iconbg emailfield">
               <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
            </div>
         </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg mobilefield">
                <input type="text" name="mobile" placeholder="Mobile Number" class="form-control">
             </div>
          </div>
         <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg locfield">
                <select class="form-control">
                   <option value="">City</option>
                   <option>Jaipur</option>
                </select>
             </div>
          </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg locfield">
                <input type="text" name="address" placeholder="Address" class="form-control">
             </div>
          </div>
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
                <input type="Password" name="pin_code" placeholder="Pin Code" class="form-control"> 
             </div>
          </div> 
          <div class="formrow">
             <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
                <input type="text" name="gst_no" placeholder="GST No (Optional)" class="form-control">
             </div>
          </div>
          <div class="formrow">
            <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
               <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password" >
            </div>
         </div>
       
         <div class="formrow">
           <div class="col-lg-12 col-md-12 col-sm-12 iconbg passfield">
              <input  id="password-confirm" type="password" class="form-control" type="Password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
              <a href="#" class="iconviewpass"></a>
           </div>
        </div> 
          <div class="row martop10">
             <div class="col-md-12" style="text-align:center;">
               {{-- <button  class="btn  btn-pink otp-modal " type="button" data-toggle="modal" data-target="#OTPModal" style="cursor: pointer;">Get OTP</button> --}}
               <button  class="btn  btn-pink" type="submit" style="cursor: pointer;">Submit</button>
             </div>
             <div class="col-md-12 signuplink">Do you have an account? <a href="JavaScript:void(0);" data-toggle="modal" data-target="#loginModal" class="signin-btn">Sign In</a></div>
          </div>
       </form>
    </div>

</div> 
</div> 

                      
                     
                      
                      
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 </div>
 <!-- Sign Up Modal end here -->
@auth
<div class="modal fade" id="BecomeWholesaler" tabindex="-1" role="dialog" aria-labelledby="BecomeWholesalerLabel" aria-hidden="true">
   <div class="modal-dialog  " role="document">
      <div class="modal-content">
         <div class="modal-body wholesaler_modal">
            <div class="row">
               <div class="col-md-12">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <div class="pad15">
                     <h2 class="modal_title">Become Wholesaler</h2>
                     <div class="col-md-12">
                        <form  method="GET" action="{{ route('user.become.wholesaler',auth()->user()->id) }}">
                          @csrf
                           <div class="formrow">
                              <label class="label-title">Company Name</label>
                              <div class="icon"><i class="far fa-envelope"></i></div>
                              <input id="company_name" type="text" placeholder="Company Name" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" required autocomplete="company_name" autofocus>                    
                           </div>
                           <div class="formrow">
                              <label class="label-title">Contact Person</label>
                              <input type="text" name="contact_person"  placeholder="Contact Person (Optional)"  class="form-control">
                                  
                           </div>
                           <div class="formrow">
                              <label class="label-title"> Address</label>
                              <input type="text" name="address"   placeholder="Address"  class="form-control">
                                  
                           </div>
                           <div class="formrow">
                              <label class="label-title">Pin Code</label>
                              <input type="text" name="pin_code"  placeholder="Pin Code"  class="form-control">
                                  
                           </div>
                           <div class="formrow">
                              <label class="label-title">GST No</label>
                              <input type="text" name="gst_no"  placeholder="GST No (Optional)"  class="form-control">
                                  
                           </div>
                           <div class="formrow borbotnone">
                              <div class="row">
                                 <div class="col-md-8 text-left txt-terms" ><input type="checkbox">I accept agreen <a href="#">New Wholesaler Agreement</a></div>
                                 <div class="col-md-4 text-right txt-forgotpass" ><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#forgotpassModal" >Forgot Password</a></div>
                              </div>
                           </div>
                           <div class="row martop10">
                              <div class="col-md-12" style="text-align:center;"> <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endauth


 <!-- Login Modal start here -->
 <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog  " role="document">
       <div class="modal-content">
          <div class="modal-body login_modal">
             <div class="row">
                <div class="col-md-12">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                   </button>
                   <div class="pad15">
                      <h2 class="modal_title">Member Sign In</h2>
                      <div class="loginform-subtitle">Enter registered mobile number OR email id to login</div>
                      <div class="formrow martop15 borbotnone">
                         <div class="col-md-12 text-center">
                           <a href="{{ route('social.login', ['provider' => 'facebook']) }}" class="btn btn-social btnfb facebook">
                              <i class="fab fa-facebook-f"></i> <span class="divider"></span> Login with Facebook
                          </a>
                           
                           <a href="{{ route('social.login', ['provider' => 'google']) }}" class="btn btn-social btn-gmail google">
                              <i class="fab fa-google"></i> <span class="divider"></span> Login with Google
                          </a>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <form  method="POST" action="{{ route('login') }}">
                           @csrf
                            <div class="formrow">
                               <label class="label-title">Mobile Number, Email ID</label>
                               <div class="icon"><i class="far fa-envelope"></i></div>
                               <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                               @error('email')
                                   <span class="invalid-feedback" role="alert">
                                       <strong>{{ $message }}</strong>
                                   </span>
                               @enderror                     
                            </div>
                            <div class="formrow">
                               <label class="label-title">Password</label>
                               <div class="icon" style="font-size:25px;">***</div>
                               <input type="password" name="password" value="" class="form-control">
                                   
                            </div>
                            <div class="formrow borbotnone">
                               <div class="row">
                                  <div class="col-md-8 text-left txt-terms" ><input type="checkbox">I accept agreen <a href="#">New Customer Agreement</a></div>
                                  <div class="col-md-4 text-right txt-forgotpass" ><a href="JavaScript:Void(0);" data-toggle="modal" data-target="#forgotpassModal" >Forgot Password</a></div>
                               </div>
                            </div>
                            <div class="row martop10">
                               <div class="col-md-12" style="text-align:center;"> <button type="submit" class="btn btn-primary">
                                 {{ __('Login') }}
                             </button>
                               </div>
                               <div class="col-md-12 signuplink">Don't have an account? <a name="submit" class="" type="button" href="#">Sign Up</a></div>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <!-- Login Modal end here -->
 <!-- Header -->
 <header class="header">
    <section id="navbar">
       <div class="header_main ">
          <div class="container">
             <div class="row">
                <div style="padding-right: 0" class="col-lg-12" style="position: unset;">
                   <div class="col-lg-2">
                      <div class="logo">
                         <a href="#"><img class="img-fluid" src="{{static_asset('frontend/images/logo.png')}}" alt="" width="150" /></a>
                      </div>
                   </div>
                   <div class="col-lg-8 col-md-8" style="position: unset;">
                      <nav class="main_nav">
                         <div class="menu-container">
                            <div class="menu">
                               <ul>
                                  <li><a class="menu1" href="#">Home</a></li>
                                  <li>
                                     <a class="menu2" href="detail_page.html">Product</a>
                                     <ul style="display:none;">
                                        <li>
                                           <div class="menu-inner-h" href="#">Best Buying</div>
                                           <ul>
                                              <li><a href="#">Lidership</a></li>
                                              <li><a href="#">History</a></li>
                                              <li><a href="#">Locations</a></li>
                                              <li><a href="#">Careers</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <div class="menu-inner-h" href="#">Research</div>
                                           <ul>
                                              <li><a href="#">Undergraduate</a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <div class="menu-inner-h" href="#">Research</div>
                                           <ul>
                                              <li><a href="#">Undergraduate</a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <div class="menu-inner-h" href="#">Research</div>
                                           <ul>
                                              <li><a href="#">Undergraduate</a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                     </ul>
                                  </li>
                                  <li><a class="menu3" href="myaccount.html">News</a>
                                  </li>
                                  <li>
                                     <a class="menu4" href="category.html">Categories</a>
                                     <ul>
                                        <li>
                                           <a class="menu-inner-h2" href="#">Best Buying</a>
                                           <ul>
                                              <li><a href="#">Lidership</a></li>
                                              <li><a href="#">History</a></li>
                                              <li><a href="#">Locations</a></li>
                                              <li><a href="#">Careers</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <a class="menu-inner-h2" href="#">Research</a>
                                           <ul>
                                              <li><a href="#">Undergraduate </a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <a class="menu-inner-h2" href="#">Research</a>
                                           <ul>
                                              <li><a href="#">Undergraduate </a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                        <li class="menures">
                                           <a class="menu-inner-h2" href="#">Research</a>
                                           <ul>
                                              <li><a href="#">Undergraduate </a></li>
                                              <li><a href="#">Masters </a></li>
                                              <li><a href="#">Funding</a></li>
                                           </ul>
                                        </li>
                                     </ul>
                                  </li>
                                  <li><a class="menu4" href="category.html">Categories</a></li>
                                  <li><a class="menu4" href="category.html">Categories</a></li>
                                  <li><a class="menu4" href="category.html">Categories</a></li>
                               </ul>
                            </div>
                         </div>
                      </nav>
                   </div>
                   <div class="col-md-2  header-rgt-panel" style="position: relative;">
                      <div style="padding-right: 0">
                         <div class="header-rgt-menu">
                            <ul>
                               <li>
                                  <a href="#"><i style="font-size: 18px; margin-right:5px;" class="fas fa-search"></i> <span style="font-family: 'Righteous', cursive; color:#000; font-size:12px;">Search</span></a>
                                  <div class="dropdown-content search-dropdown">
                                     <input type="text" name="" placeholder="Search goes here" />
                                     <button><i class="fas fa-search"></i></button>
                                  </div>
                               </li>
                               <li>
                                  <a href="#"><i style="font-size: 18px; margin-top: 5px;" class="far fa-user"></i></a>
                                  <div class="dropdown-content account-dropdown">
                                     @auth

                                     <ul class="user-details">
                                       <li>
                                          <p class="text-left"><i class="fa fa-user-circle theme-color user-icon" aria-hidden="true"></i>{{auth()->user()->name}}</p>
                                       </li>
                                       <li>
                                          <p class="text-left"><i class="fa fa-table theme-color user-icon" aria-hidden="true"></i> Dashboard</p>
                                       </li>
                                       @if (isset(auth()->user()->wholesaler))
                                       @if (auth()->user()->wholesaler->status==0)
                                       <li>
                                          <a href="#" onclick="switchfunc(1)"> <p class="text-left"><i class="fa fa-power-off theme-color user-icon" aria-hidden="true"></i> Switch to Wholesaler</p></a>
                                       </li>
                                       @else
                                       <li>
                                          <a href="#" onclick="switchfunc(0)"> <p class="text-left"><i class="fa fa-power-off theme-color user-icon" aria-hidden="true"></i> Switch to User</p></a>
                                       </li>
                                       @endif
                                       @else
                                       <li>
                                          {{-- <button type="button"  data-toggle="modal" data-target="#loginModal"> <p class="text-left"><i class="fa fa-power-off theme-color user-icon" aria-hidden="true"></i> Become a Wholesaler</p></button> --}}
                                          <a href="#" data-toggle="modal" data-target="#BecomeWholesaler"> <p class="text-left"><i class="fa fa-power-off theme-color user-icon" aria-hidden="true"></i> Become a Wholesaler</p></a>
                                          
                                       </li>
                                       @endif
                                       <li>
                                         <a href="{{route('user.logout')}}"> <p class="text-left"><i class="fa fa-power-off theme-color user-icon" aria-hidden="true"></i> Logout</p></a>
                                       </li>
                                    </ul>
                                    @else
                                    <ul>
                                       <li>
                                          <a href="#"><img src="{{asset('frontend/images/facebook-svg.svg')}}" alt="" width="220" /></a>
                                       </li>
                                       <li>
                                          <a href="#"><img src="{{asset('frontend/images/google-svg.svg')}}" alt="" width="220" /></a>
                                       </li>
                                       <li><strong>OR</strong></li>
                                       <li><button type="button" class="btn-grey" data-toggle="modal" data-target="#loginModal">Sign In</button></li>
                                       <li><button type="button" class="btn-grey" data-toggle="modal" data-target="#registerModal">Sign Up</button></li>
                                    </ul>
                                     @endauth
                                  
                                     
                                  </div>
                               </li>
                               <li>
                                  <a href="#"><i style="font-size: 18px; margin-top: 5px;" class="far fa-heart"></i></a>
                                  <div class="dropdown-content wishlist-content">
                                     <p><i style="font-size: 18px; margin-top: 5px;" class="far fa-heart"></i>
                                        <br/>Love Something ? Save it here
                                     </p>
                                  </div>
                               </li>
                               <li>
                                 <div class="d-none d-lg-block  align-self-stretch ml-3 mr-0" data-hover="dropdown">
                                    <div class="nav-cart-box dropdown h-100" id="cart_items">
                                        @include('frontend.partials.cart')
                                    </div>
                                </div>
                               </li>
                            </ul>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>
 </header>