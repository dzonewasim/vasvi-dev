 <!-- Footer -->
 <div class="characteristics">
    <div class="container">
       <div class="row">
          <!-- Char. Item -->
          <div class="col-lg-3 col-md-6 char_col">
             <div class="char_item d-flex flex-row align-items-center justify-content-start">
                <div class="char_icon"><i class="fas fa-check-circle"></i></div>
                <div class="char_content">
                   <div class="char_title">100% Authorised
                      <br> product
                   </div>
                </div>
             </div>
          </div>
          <!-- Char. Item -->
          <div class="col-lg-3 col-md-6 char_col">
             <div class="char_item d-flex flex-row align-items-center justify-content-start">
                <div class="char_icon"><i class="fas fa-truck"></i></div>
                <div class="char_content">
                   <div class="char_title">15 Days Return </div>
                </div>
             </div>
          </div>
          <!-- Char. Item -->
          <div class="col-lg-3 col-md-6 char_col">
             <div class="char_item d-flex flex-row align-items-center justify-content-start">
                <div class="char_icon"><i class="fas fa-headphones"></i></div>
                <div class="char_content">
                   <div class="char_title"> Quick support </div>
                </div>
             </div>
          </div>
          <!-- Char. Item -->
          <div class="col-lg-3 col-md-6 char_col">
             <div class="char_item d-flex flex-row align-items-center justify-content-start">
                <div class="char_icon"><i class="fas fa-handshake"></i></div>
                <div class="char_content">
                   <div class="char_title">Free Delivery</div>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
 <section class="footer-top">
    <div class="container">
       <div class="row">
          <div class=" col-md-4">
             <div class="footer_column">
                <div class="footer_title" style="color: black;">Secure Pay</div>
                <ul class="Secure-pay">
                   <li>
                      <a href="#"><img class="img-responsive" src="{{static_asset('frontend/images/SSL_Secure.png" alt="payments')}}" width="80px"></a>
                   </li>
                   <li>
                      <a href="#"><img class="img-responsive" src="{{static_asset('frontend/images/ssl-secure-shopping.png')}}" width="80px" alt="payments"></a>
                   </li>
                </ul>
             </div>
          </div>
          <div class="col-lg-4">
             <div class="footer_column footer-contact">
                <div class="footer_title" style="color: black;">Any questions</div>
                <div style="padding-top: 20px" class="media">
                   <div class="icon-support">
                      <i class="fab fa-whatsapp"></i>
                      <i class="fas fa-phone"></i>
                   </div>
                   <div class="support-text">
                      +91 6376681424
                   </div>
                </div>
                <div class="media">
                   <div class="icon-support"><i class="fas fa-envelope"></i></div>
                   <div class="support-text">
                      care@vasvi.in
                   </div>
                </div>
             </div>
          </div>
          <div class=" col-md-4">
             <div class="footer_column">
                <div class="footer_title" style="color: black;">Follow us</div>
                <div class="footer_social">
                   <ul>
                      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                      <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                      <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                      <li><a href="#"><i class="fab fa-google"></i></a></li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
    </div>
 </section>
 <button  class="float"   >
 <i class="fa fa-comments my-float" id="chatbox" aria-hidden="true"></i>
 </button>
 <div id="myForm" class="hide">
    <form action="/echo/html/" id="popForm" method="get">
       <div class="">
          <label class="chat-label" for="name">Name:</label>
          <input type="text" name="name" id="name" class="form-control input-md">
          <label class="chat-label" for="email">Email:</label>
          <input type="email" name="email" id="email" class="form-control input-md">
          <label class="chat-label" for="phone">Phone:</label>
          <input type="phone" name="email" id="phone" class="form-control input-md"> 
          <label class="chat-label" for="about">Message:</label>
          <textarea rows="3" name="about" id="about" class="form-control input-md"></textarea>
          <button type="button" class="btn btn-submit-review btn-block mt-4" data-loading-text="Sending info.."><em class="icon-ok"></em> SUBMIT</button>
       </div>
    </form>
 </div>
 <footer class="footer">
    <div class="container">
       <div class="row">
          <div class="col-lg-3">
             <div class="footer_column">
                <div class="footer_title">Customer</div>
                <ul class="footer_list">
                   <li><a href="#">Girl Dreess & Tablets</a></li>
                   <li><a href="#">TV & Audio</a></li>
                   <li>
                      <a href="#" onclick="document.getElementById('id03').style.display='block'"><i class="fas fa-headset"></i> Help / Support</a>
                   </li>
                </ul>
                <div class="footer_subtitle">Gadgets</div>
                <ul class="footer_list">
                   <li><a href="#">Car Electronics</a></li>
                </ul>
             </div>
          </div>
          <div class="col-lg-3">
             <div class="footer_column">
                <div class="footer_title">Policies</div>
                <ul class="footer_list">
                   <li><a href="#">My Account</a></li>
                   <li><a href="#">Order Tracking</a></li>
                   <li><a href="#">Wish List</a></li>
                   <li><a href="#">Customer Services</a></li>
                   <li><a href="#">Returns / Exchange</a></li>
                   <li><a href="#">FAQs</a></li>
                   <li><a href="#">Product Support</a></li>
                </ul>
             </div>
          </div>
          <div class="col-lg-3">
             <div class="footer_column">
                <div class="footer_title">Vasvi and store</div>
                <ul class="footer_list">
                   <li><a href="#">My Account</a></li>
                   <li><a href="#">Order Tracking</a></li>
                   <li><a href="#">Wish List</a></li>
                   <li><a href="#">Customer Services</a></li>
                   <li><a href="#">Returns / Exchange</a></li>
                   <li><a href="#">FAQs</a></li>
                   <li><a href="#">Product Support</a></li>
                </ul>
             </div>
          </div>
          <div class="col-lg-3 footer_col">
             <div class="footer_column">
                <div class="footer_title">Contact Info</div>
                <ul class="contact-info">
                   <li>
                      <p><i class="fas fa-map-marker-alt"></i> <strong>Address:</strong><br>G/13, Lorem ipsum Industrial Area,<br> Jaipur, Rajasthan</p>
                   </li>
                   <li>
                      <p><i class="fas fa-mobile-alt"></i><strong>Phone:</strong> <a href="tel:+911414037596"> (+91) 141 4037596</a> - <a href="tel:+911414029596">4029596</a></p>
                   </li>
                   <li> <a href="#" target="_blank"><img alt="whatsapp" src="{{static_asset('frontend/images/whatsapp-icon.png')}}" style="margin-right: 7px;" width="15"><strong>Whatsapp:</strong> (+91) 9314966969</a></li>
                   <li>
                      <p><i class="fas fa-envelope"></i><strong>Email:</strong><a href="mailto:info@vasvi.com">info@vasvi.com</a></p>
                   </li>
                   <li>
                      <p><i class="far fa-clock"></i><strong>Working Days/Hours:</strong><br>Mon - Sat / 10:00AM - 7:00PM</p>
                   </li>
                </ul>
             </div>
          </div>
       </div>
    </div>
 </footer>
 <div class="copyright">
    <div class="container">
       <div class="row">
          <div class="col">
             <div class="copyright_container d-flex flex-sm-row flex-column">
                <div style="text-align: center;" class="copyright_content col-md-12">
                   <p>Copyright @ 2018 vasvi.in - All Right Reserved | <a style="color: #979797; border-bottom: 0" href="http://dzoneindia.org/" target="_blank">Powered By dzone India.</a></p>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>