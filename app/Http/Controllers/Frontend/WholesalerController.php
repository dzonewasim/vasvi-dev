<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\User;
use App\Wholesaler;
use Illuminate\Http\Request;

class WholesalerController extends Controller
{
    public function become($id,Request $request){
       $user= User::find($id);
       $user->company_name=$request->company_name;
       $user->contact_person=$request->contact_person;
       $user->address=$request->address;
       $user->pin_code=$request->pin_code;
       $user->gst_no=$request->gst_no;
       $user->save();
     
   Wholesaler::create([
     'user_id'=>$id,
      'status'=>1
     ]);
     return redirect('home');
    }

    public function switch(Request $request){
     
        auth()->user()->wholesaler->status=$request->status;
        auth()->user()->wholesaler->save();
   
      
      return response()->json('success',200);
     }
}
