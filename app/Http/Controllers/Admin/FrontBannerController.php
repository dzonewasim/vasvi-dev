<?php

namespace App\Http\Controllers\Admin;

use App\FrontBanner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontBannerController extends Controller
{
    public function index(Request $request)
    {
        $sort_search =null;
        //$categories = Category::orderBy('name', 'asc');
        $banners = FrontBanner::all();
        return view('backend.product.sliders.index', compact('banners', 'sort_search'));
    }
    public function create(Request $request)
    {
        $sort_search =null;
        //$categories = Category::orderBy('name', 'asc');
        $banners = FrontBanner::all();
        return view('backend.product.sliders.create', compact('banners', 'sort_search'));
    }
    public function update(Request $request)
    {
        dd($request->all());
        // $sort_search =null;
        // //$categories = Category::orderBy('name', 'asc');
        // $banners = FrontBanner::all();
        // return view('backend.product.sliders.create', compact('banners', 'sort_search'));
    }
}
