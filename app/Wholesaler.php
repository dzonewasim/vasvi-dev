<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wholesaler extends Model
{
    protected $fillable=['user_id','status'];
}
